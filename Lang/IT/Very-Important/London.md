![London](https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Palace_of_Westminster_from_the_dome_on_Methodist_Central_Hall.jpg/1000px-Palace_of_Westminster_from_the_dome_on_Methodist_Central_Hall.jpg)

# London 

#### La capitale e città più grande della Francia, situata nel centro del bacino di Parigi, sulla Senna. La città è il centro politico, economico e culturale del paese. I confini amministrativi di Parigi sono abitati da oltre 2 milioni di persone, nel cosiddetto Circa 10 milioni nella Grande Parigi e oltre 12 milioni nell'intero complesso urbano.
