![New York](https://images.musement.com/cover/0002/49/thumb_148242_cover_header.jpeg?w=1200&h=630&q=60&fit=crop)

# New York 

#### La città più popolosa degli Stati Uniti e al tempo stesso il centro di uno degli agglomerati più popolati al mondo. New York ha un'influenza significativa su business globale, finanza, media, arte, moda, ricerca, tecnologia, istruzione e intrattenimento.
