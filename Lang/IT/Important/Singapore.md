![Singapur](https://i.content4travel.com/cms/img/u/kraj/2/singapur_0.jpg?version=180830-12)

# Singapur

#### Città-stato situata vicino all'estremità meridionale della penisola malese, nel sud-est asiatico. Fu fondata dagli inglesi nel 1819 e conquistò l'indipendenza il 9 agosto 1965. Il suo nome deriva da due parole sanscrite: simha e pura, da qui il nome a volte usato della città del leone.
