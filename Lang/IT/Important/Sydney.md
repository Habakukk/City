![Sydney](https://www.sydney.com/sites/sydney/files/styles/full_height_image/public/2018-02/syd-1-1_0.jpg?itok=UawSK9dM)

# Sydney 
#### La più grande città dell'Australia e dell'Oceania con una popolazione di oltre 5,1 milioni di abitanti, anche la capitale del New South Wales. Fondata nel 1788, Sydney è un importante centro finanziario, commerciale, di trasporto, culturale e turistico, con uno status metropolitano di importanza mondiale.
