![Tokio](https://www.moneysense.ca/wp-content/uploads/2018/06/tokyo-japan-shutterstock-810x445.jpg)

# Tokio 
#### La capitale e la più grande città del Giappone, situata sulla costa sud-orientale di Honshu e allo stesso tempo la più grande area metropolitana del mondo a livello di 38 305 000 abitanti. Il nome Tōkyō significa "capitale orientale". Fino al 1868, la città era chiamata Edo.
