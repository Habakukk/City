![Beijing](https://nouvelles.umontreal.ca/fileadmin/_processed_/csm_20180124_pekin-chine_35a2571e88.jpg)

# Beijing 
#### Capitale della Repubblica popolare cinese e una delle quattro città separate, che riporta direttamente alle autorità del paese. Da nord, ovest e sud confina con la provincia di Hebei, e da est con la città separata di Tianjin.
