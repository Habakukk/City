![Hong Kong](https://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2017/09/20/104720584-02-_shutterstock_141266803.1910x1000.jpg)

# Hong Kong 
#### Regione amministrativa ufficialmente speciale di Hong Kong - una regione amministrativa speciale della Repubblica popolare cinese, situata sul Mar Cinese Meridionale, nel delta del Fiume delle Perle. Negli anni 1842-1997, la tenuta britannica.
