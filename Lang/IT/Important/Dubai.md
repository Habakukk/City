![Dubaj](https://gfx.radiozet.pl/var/radiozet/storage/images/podroze-radia-zet/dubaj-8-rzeczy-o-ktorych-warto-wiedziec-przed-podroza-poradnik/782114-1-pol-PL/Dubaj-8-rzeczy-o-ktorych-warto-wiedziec-przed-podroza-PORADNIK_article.jpg)

# Dubaj 

#### La più grande città negli Emirati Arabi Uniti. La capitale dell'emirato di Dubai. Situato sulla costa meridionale del Golfo Persico. Nel 2018 contava circa 3,1 milioni di abitanti.
