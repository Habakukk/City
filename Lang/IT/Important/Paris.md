![Paris](https://photos.mandarinoriental.com/is/image/MandarinOriental/paris-2017-home?wid=2880&hei=1280&fmt=jpeg&crop=9,336,2699,1200&anchor=1358,936&qlt=75,0&fit=wrap&op_sharpen=0&resMode=sharp2&op_usm=0,0,0,0&iccEmbed=0&printRes=72)

# Paryż 

#### La capitale e città più grande della Francia, situata nel centro del bacino di Parigi, sulla Senna. La città è il centro politico, economico e culturale del paese. I confini amministrativi di Parigi sono abitati da oltre 2 milioni di persone, nel cosiddetto Circa 10 milioni nella Grande Parigi e oltre 12 milioni nell'intero complesso urbano.
