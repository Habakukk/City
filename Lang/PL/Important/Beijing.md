![Beijing](https://nouvelles.umontreal.ca/fileadmin/_processed_/csm_20180124_pekin-chine_35a2571e88.jpg)

# Beijing 
#### Stolica Chińskiej Republiki Ludowej oraz jedno z czterech miast wydzielonych, podlegające bezpośrednio władzom kraju. Od północy, zachodu i południa graniczy z prowincją Hebei, a od wschodu z miastem wydzielonym Tiencin.
