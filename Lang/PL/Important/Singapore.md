![Singapur](https://i.content4travel.com/cms/img/u/kraj/2/singapur_0.jpg?version=180830-12)

# Singapur

#### Miasto-państwo położone w pobliżu południowego krańca Półwyspu Malajskiego, w południowo-wschodniej Azji. Został założony przez Brytyjczyków w 1819 i uzyskał niepodległość 9 sierpnia 1965 roku. Jego nazwa pochodzi od dwóch sanskryckich słów: simha i pura, stąd niekiedy stosowana nazwa Miasto Lwa.
