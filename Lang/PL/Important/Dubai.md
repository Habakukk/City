![Dubaj](https://gfx.radiozet.pl/var/radiozet/storage/images/podroze-radia-zet/dubaj-8-rzeczy-o-ktorych-warto-wiedziec-przed-podroza-poradnik/782114-1-pol-PL/Dubaj-8-rzeczy-o-ktorych-warto-wiedziec-przed-podroza-PORADNIK_article.jpg)

# Dubaj 

#### Największe miasto w Zjednoczonych Emiratach Arabskich. Stolica emiratu Dubaj. Położone na południowym wybrzeżu Zatoki Perskiej. W 2018 liczyło ok. 3,1 mln mieszkańców.
