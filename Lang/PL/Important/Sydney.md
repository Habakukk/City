![Sydney](https://www.sydney.com/sites/sydney/files/styles/full_height_image/public/2018-02/syd-1-1_0.jpg?itok=UawSK9dM)

# Sydney 
#### Największe miasto Australii i Oceanii z liczbą ludności wynoszącą ponad 5,1 mln mieszkańców, także stolica stanu Nowa Południowa Walia. Założone w 1788 roku, Sydney jest znaczącym centrum finansowym, handlowym, transportowym, kulturalnym i turystycznym, mając status metropolii o znaczeniu globalnym.
