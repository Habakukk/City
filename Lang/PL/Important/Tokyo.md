![Tokio](https://www.moneysense.ca/wp-content/uploads/2018/06/tokyo-japan-shutterstock-810x445.jpg)

# Tokio 
#### Stolica i największe miasto Japonii, położone na południowo-wschodnim wybrzeżu Honsiu i zarazem największy obszar metropolitalny na świecie na poziomie 38 305 000 mieszkańców. Nazwa Tōkyō oznacza „Wschodnią Stolicę”. Do 1868 roku miasto nazywało się Edo.
