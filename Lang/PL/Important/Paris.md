![Paris](https://photos.mandarinoriental.com/is/image/MandarinOriental/paris-2017-home?wid=2880&hei=1280&fmt=jpeg&crop=9,336,2699,1200&anchor=1358,936&qlt=75,0&fit=wrap&op_sharpen=0&resMode=sharp2&op_usm=0,0,0,0&iccEmbed=0&printRes=72)

# Paryż 

#### Stolica i największe miasto Francji, położone w centrum Basenu Paryskiego, nad Sekwaną. Miasto stanowi centrum polityczne, ekonomiczne i kulturalne kraju. W granicach administracyjnych Paryża zamieszkuje ponad 2 mln osób, w tzw. Wielkim Paryżu ok. 10 mln, a w całym zespole miejskim ponad 12 mln.
