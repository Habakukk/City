![New York](https://images.musement.com/cover/0002/49/thumb_148242_cover_header.jpeg?w=1200&h=630&q=60&fit=crop)

# New York 

#### Najludniejsze miasto w Stanach Zjednoczonych, a zarazem centrum jednej z najludniejszych aglomeracji na świecie. Nowy Jork wywiera znaczący wpływ na światowy biznes, finanse, media, sztukę, modę, badania naukowe, technologię, edukację oraz rozrywkę.
