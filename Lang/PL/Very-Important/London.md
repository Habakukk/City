![London](https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Palace_of_Westminster_from_the_dome_on_Methodist_Central_Hall.jpg/1000px-Palace_of_Westminster_from_the_dome_on_Methodist_Central_Hall.jpg)

# London 

#### Stolica i największe miasto Francji, położone w centrum Basenu Paryskiego, nad Sekwaną. Miasto stanowi centrum polityczne, ekonomiczne i kulturalne kraju. W granicach administracyjnych Paryża zamieszkuje ponad 2 mln osób, w tzw. Wielkim Paryżu ok. 10 mln, a w całym zespole miejskim ponad 12 mln.
