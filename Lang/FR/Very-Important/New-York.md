![New York](https://images.musement.com/cover/0002/49/thumb_148242_cover_header.jpeg?w=1200&h=630&q=60&fit=crop)

# New York 

#### La ville la plus peuplée des États-Unis et en même temps le centre de l'une des agglomérations les plus peuplées du monde. New York a une influence significative sur le commerce mondial, la finance, les médias, l'art, la mode, la recherche, la technologie, l'éducation et le divertissement.
