![Beijing](https://nouvelles.umontreal.ca/fileadmin/_processed_/csm_20180124_pekin-chine_35a2571e88.jpg)

# Beijing 
#### Capitale de la République populaire de Chine et l'une des quatre villes séparées, se rapportant directement aux autorités du pays. Au nord, à l'ouest et au sud, elle borde la province de Hebei et à l'est avec la ville séparée de Tianjin.
