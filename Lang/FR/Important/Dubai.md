![Dubaj](https://gfx.radiozet.pl/var/radiozet/storage/images/podroze-radia-zet/dubaj-8-rzeczy-o-ktorych-warto-wiedziec-przed-podroza-poradnik/782114-1-pol-PL/Dubaj-8-rzeczy-o-ktorych-warto-wiedziec-przed-podroza-PORADNIK_article.jpg)

# Dubaj 

#### La plus grande ville des Emirats Arabes Unis. La capitale de l'émirat de Dubaï. Situé sur la côte sud du golfe Persique. En 2018, il comptait environ 3,1 millions d'habitants.
