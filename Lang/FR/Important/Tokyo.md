![Tokio](https://www.moneysense.ca/wp-content/uploads/2018/06/tokyo-japan-shutterstock-810x445.jpg)

# Tokio 
#### La capitale et la plus grande ville du Japon, située sur la côte sud-est de Honshu, est en même temps la plus grande région métropolitaine au monde avec 38 305 000 habitants. Le nom Tōkyō signifie "capitale de l'Est". Jusqu'en 1868, la ville s'appelait Edo.
