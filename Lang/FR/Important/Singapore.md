![Singapur](https://i.content4travel.com/cms/img/u/kraj/2/singapur_0.jpg?version=180830-12)

# Singapur

#### Ville-État située près de la pointe sud de la péninsule malaise, en Asie du sud-est. Il a été fondé par les Britanniques en 1819 et a acquis son indépendance le 9 août 1965. Son nom vient de deux mots sanscrits: simha et pura, d'où le nom parfois utilisé de la Cité du Lion.
