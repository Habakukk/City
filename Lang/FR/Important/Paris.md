![Paris](https://photos.mandarinoriental.com/is/image/MandarinOriental/paris-2017-home?wid=2880&hei=1280&fmt=jpeg&crop=9,336,2699,1200&anchor=1358,936&qlt=75,0&fit=wrap&op_sharpen=0&resMode=sharp2&op_usm=0,0,0,0&iccEmbed=0&printRes=72)

# Paryż 

#### Capitale et plus grande ville de France, située au centre du Bassin parisien, sur la Seine. La ville est le centre politique, économique et culturel du pays. Les frontières administratives de Paris sont habitées par plus de 2 millions de personnes, dans le soi-disant Environ 10 millions dans le Grand Paris et plus de 12 millions dans l'ensemble du complexe urbain.
