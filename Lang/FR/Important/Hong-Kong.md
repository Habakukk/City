![Hong Kong](https://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2017/09/20/104720584-02-_shutterstock_141266803.1910x1000.jpg)

# Hong Kong 
#### Région administrative officiellement spéciale de Hong Kong - région administrative spéciale de la République populaire de Chine, située sur la mer de Chine méridionale, dans le delta de la rivière des Perles. Dans les années 1842-1997, le domaine britannique.
