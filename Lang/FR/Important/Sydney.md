![Sydney](https://www.sydney.com/sites/sydney/files/styles/full_height_image/public/2018-02/syd-1-1_0.jpg?itok=UawSK9dM)

# Sydney 
#### La plus grande ville d’Australie et d’Océanie avec une population de plus de 5,1 millions d’habitants est également la capitale de la Nouvelle-Galles du Sud. Fondée en 1788, Sydney est un centre financier, commercial, de transport, culturel et touristique important, doté d'un statut métropolitain d'importance mondiale.
