![London](https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Palace_of_Westminster_from_the_dome_on_Methodist_Central_Hall.jpg/1000px-Palace_of_Westminster_from_the_dome_on_Methodist_Central_Hall.jpg)

# London 

#### Die Hauptstadt und größte Stadt Frankreichs liegt im Zentrum des Pariser Beckens an der Seine. Die Stadt ist das politische, wirtschaftliche und kulturelle Zentrum des Landes. Die Verwaltungsgrenzen von Paris werden von über 2 Millionen Menschen bewohnt, in den sogenannten Etwa 10 Millionen im Großen Paris und über 12 Millionen im gesamten Stadtkomplex.
