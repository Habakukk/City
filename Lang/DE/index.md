---
title: "Die wichtigsten Städte der Welt"
---

#### Stadt auswählen:

<div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown">Sehr wichtige Stadt<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">          
            <li><a href="Very-Important/London">London</a></li>
            <li><a href="Very-Important/New-York">New York</a></li>
          </ul>
        </li>
     <li class="dropdown">
          <a class="dropdown-toggle">Wichtige Stadt<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">          
            <li><a href="Important/Hong-Kong">Hong Kong</a></li>
            <li><a href="Important/Beijing">Beijing</a></li>
            <li><a href="Important/Singapore">Singapore</a></li>
            <li><a href="Important/Sydney">Sydney</a></li>
            <li><a href="Important/Dubai">Dubai</a></li>
            <li><a href="Important/Paris">Paris</a></li>
            <li><a href="Important/Tokyo">Tokyo</a></li>
          </ul>
        </li>
      </ul>
    </div>

##### Quelle: [https://en.wikipedia.org/wiki/Global_city](https://en.wikipedia.org/wiki/Global_city)
