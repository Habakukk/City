![Sydney](https://www.sydney.com/sites/sydney/files/styles/full_height_image/public/2018-02/syd-1-1_0.jpg?itok=UawSK9dM)

# Sydney 
#### Die größte Stadt Australiens und Ozeaniens mit über 5,1 Millionen Einwohnern ist auch die Hauptstadt von New South Wales. Sydney wurde 1788 gegründet und ist ein bedeutendes Finanz-, Handels-, Transport-, Kultur- und Tourismuszentrum mit weltstädtischem Status.
