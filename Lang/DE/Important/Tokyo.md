![Tokio](https://www.moneysense.ca/wp-content/uploads/2018/06/tokyo-japan-shutterstock-810x445.jpg)

# Tokio 
#### Die Hauptstadt und größte Stadt Japans liegt an der Südostküste von Honshu und gleichzeitig die größte Metropolregion der Welt mit 38 305 000 Einwohnern. Der Name Tōkyō bedeutet "östliche Hauptstadt". Bis 1868 hieß die Stadt Edo.
