![Beijing](https://nouvelles.umontreal.ca/fileadmin/_processed_/csm_20180124_pekin-chine_35a2571e88.jpg)

# Beijing 
#### Hauptstadt der Volksrepublik China und eine der vier getrennten Städte, die direkt den Behörden des Landes unterstellt sind. Im Norden, Westen und Süden grenzt es an die Provinz Hebei und im Osten an die getrennte Stadt Tianjin.
