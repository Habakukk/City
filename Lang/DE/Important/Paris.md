![Paris](https://photos.mandarinoriental.com/is/image/MandarinOriental/paris-2017-home?wid=2880&hei=1280&fmt=jpeg&crop=9,336,2699,1200&anchor=1358,936&qlt=75,0&fit=wrap&op_sharpen=0&resMode=sharp2&op_usm=0,0,0,0&iccEmbed=0&printRes=72)

# Paryż 

#### Die Hauptstadt und größte Stadt Frankreichs liegt im Zentrum des Pariser Beckens an der Seine. Die Stadt ist das politische, wirtschaftliche und kulturelle Zentrum des Landes. Die Verwaltungsgrenzen von Paris werden von über 2 Millionen Menschen bewohnt, in den sogenannten Etwa 10 Millionen im Großen Paris und über 12 Millionen im gesamten Stadtkomplex.
