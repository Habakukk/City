![Singapur](https://i.content4travel.com/cms/img/u/kraj/2/singapur_0.jpg?version=180830-12)

# Singapur

#### Stadtstaat in der Nähe der Südspitze der Malaiischen Halbinsel in Südostasien. Es wurde 1819 von den Briten gegründet und erlangte am 9. August 1965 die Unabhängigkeit. Sein Name stammt von zwei Sanskrit-Wörtern: Simha und Pura, daher der manchmal verwendete Name der Stadt des Löwen.
