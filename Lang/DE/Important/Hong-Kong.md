![Hong Kong](https://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2017/09/20/104720584-02-_shutterstock_141266803.1910x1000.jpg)

# Hong Kong 
#### Offizielles Sonderverwaltungsgebiet Hongkong - ein Sonderverwaltungsgebiet der Volksrepublik China, gelegen am Südchinesischen Meer im Pearl River Delta. In den Jahren 1842-1997 erfolgte der britische Nachlass.
