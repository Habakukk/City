![New York](https://images.musement.com/cover/0002/49/thumb_148242_cover_header.jpeg?w=1200&h=630&q=60&fit=crop)

# New York 

#### The most populous city in the United States, and at the same time the center of one of the most populated agglomerations in the world. New York has a significant influence on global business, finance, media, art, fashion, research, technology, education and entertainment.
