![Tokio](https://www.moneysense.ca/wp-content/uploads/2018/06/tokyo-japan-shutterstock-810x445.jpg)

# Tokio 
#### The capital and the largest city of Japan, located on the south-eastern coast of Honshu and at the same time the largest metropolitan area in the world at the level of 38 305 000 inhabitants. The name Tōkyō means "Eastern Capital". Until 1868, the city was called Edo.
