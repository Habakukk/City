![Singapur](https://i.content4travel.com/cms/img/u/kraj/2/singapur_0.jpg?version=180830-12)

# Singapur

#### City-state located near the southern tip of the Malay Peninsula, in south-east Asia. It was founded by the British in 1819 and gained independence on August 9, 1965. Its name comes from two Sanskrit words: simha and pura, hence the sometimes used name of the City of Lion.
