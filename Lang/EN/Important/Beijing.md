![Beijing](https://nouvelles.umontreal.ca/fileadmin/_processed_/csm_20180124_pekin-chine_35a2571e88.jpg)

# Beijing 
#### Capital of the People's Republic of China and one of the four separated cities, reporting directly to the country's authorities. From the north, west and south it borders with the Hebei province, and from the east with the separated town of Tianjin.
