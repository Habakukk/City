![Sydney](https://www.sydney.com/sites/sydney/files/styles/full_height_image/public/2018-02/syd-1-1_0.jpg?itok=UawSK9dM)

# Sydney 
#### The largest city of Australia and Oceania with a population of over 5.1 million, also the capital of New South Wales. Founded in 1788, Sydney is a significant financial, commercial, transport, cultural and tourist center, with metropolitan status of global significance.
