![Dubaj](https://gfx.radiozet.pl/var/radiozet/storage/images/podroze-radia-zet/dubaj-8-rzeczy-o-ktorych-warto-wiedziec-przed-podroza-poradnik/782114-1-pol-PL/Dubaj-8-rzeczy-o-ktorych-warto-wiedziec-przed-podroza-PORADNIK_article.jpg)

# Dubaj 

#### The largest city in the United Arab Emirates. The capital of the emirate of Dubai. Located on the southern coast of the Persian Gulf. In 2018, it had about 3.1 million inhabitants.
