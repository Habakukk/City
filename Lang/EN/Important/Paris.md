![Paris](https://photos.mandarinoriental.com/is/image/MandarinOriental/paris-2017-home?wid=2880&hei=1280&fmt=jpeg&crop=9,336,2699,1200&anchor=1358,936&qlt=75,0&fit=wrap&op_sharpen=0&resMode=sharp2&op_usm=0,0,0,0&iccEmbed=0&printRes=72)

# Paryż 

#### The capital and largest city of France, located in the center of the Paris Basin, on the Seine. The city is the political, economic and cultural center of the country. The administrative borders of Paris are inhabited by over 2 million people, in the so-called About 10 million in Great Paris and over 12 million in the entire urban complex.
