---
title: "Language"
---

<div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown">Choose your language<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">          
            <li><a href="Lang/EN">EN</a></li>
            <li><a href="Lang/DE">DE</a></li>
            <li><a href="Lang/FR">FR</a></li>
            <li><a href="Lang/IT">IT</a></li>
            <li><a href="Lang/PL">PL</a></li>
          </ul>
        </li>
